# README #

Museo Android App

## Developers ##
William Huang (sage7will)
Ryan Ice (ryice)
Peter Nguyen (nguyenph88)
Apollo Jain (apollojain)
Quinton Dang (qdang16)

## API ##
The API for this application was developed using Flask. you can find a video demonstration of the API <a href="https://www.youtube.com/watch?v=YD9-f7CIcPU">here</a>. 
I have also included the code for this version of the API <a href="http://pastebin.com/PKgLv2H8">here</a>. 