package com.group12.common;

import android.graphics.Bitmap;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by williamhuang on 12/1/15.
 */
public class Museum {
    private int id;
    private String description;
    private String image_url;
    private String title;
    private int recommendations;
    private List<Exhibit> exhibits;
    private Bitmap image;
    private boolean downloading;
    private boolean recommended;

    public boolean isRecommended() {
        return recommended;
    }

    public void setRecommended(boolean recommended) {
        this.recommended = recommended;
    }

    public boolean isDownloading() {
        return downloading;
    }

    public void setDownloading(boolean downloading) {
        this.downloading = downloading;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public Museum(int id, String description, String image_url, String title, int recommendations) {
        this.id = id;
        this.description = description;
        setImage_url(image_url);
        this.title = title;
        this.recommendations = recommendations;
        this.exhibits = new LinkedList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url.replaceFirst("/","");
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getRecommendations() {
        return recommendations;
    }

    public void setRecommendations(int recommendations) {
        this.recommendations = recommendations;
    }

    public List<Exhibit> getExhibits() {
        return exhibits;
    }

    public void addExhibit(Exhibit e) {
        this.exhibits.add(e);
    }

}
