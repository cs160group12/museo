package com.group12.common;

import android.graphics.Bitmap;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by williamhuang on 12/1/15.
 */
public class Exhibit {
    private int id;

    public Museum getMuseum() {
        return museum;
    }

    public void setMuseum(Museum museum) {
        this.museum = museum;
    }

    private Museum museum;
    private String description;
    private String image_url;
    private String name;
    private int recommendations;
    private List<Artifact> artifacts;
    private Bitmap image;
    private boolean downloading;
    private boolean recommended;

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public boolean isDownloading() {
        return downloading;
    }

    public void setDownloading(boolean downloading) {
        this.downloading = downloading;
    }

    public boolean isRecommended() {
        return recommended;
    }

    public void setRecommended(boolean recommended) {
        this.recommended = recommended;
    }

    public Exhibit(int id, Museum museum, String description, String image_url, String name, int recommendations) {
        this.id = id;
        this.museum = museum;
        this.description = description;
        this.image_url = image_url;
        this.name = name;
        this.recommendations = recommendations;
        this.artifacts = new LinkedList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRecommendations() {
        return recommendations;
    }

    public void setRecommendations(int recommendations) {
        this.recommendations = recommendations;
    }

    public List<Artifact> getArtifacts() {
        return artifacts;
    }

    public void addArtifact(Artifact a) {
        artifacts.add(a);
    }
}
