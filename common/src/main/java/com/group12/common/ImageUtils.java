package com.group12.common;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.util.Log;

import org.json.JSONObject;

import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by williamhuang on 12/2/15.
 */
public class ImageUtils {

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromStream(String url, int reqWidth, int reqHeight) {
        InputStream in = null;
        try{
            in = new java.net.URL(url).openStream();
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
        }

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        Rect padding = new Rect();
        BitmapFactory.decodeStream(in, padding, options);

        try{
            in = new java.net.URL(url).openStream();
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
        }

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeStream(in, padding, options);
    }

    public static void postRecommendation(final String museumURL, final int recommendation) {
        final String TAG = "postRecomm";
        new Thread( new Runnable() {
            @Override
            public void run() {
                try {
                    JSONObject obj = new JSONObject();
                    obj.put("recommendations", recommendation);

                    URL url = new URL(museumURL);
                    HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
                    httpCon.setDoOutput(true);
                    httpCon.setRequestProperty("Content-Type","application/json");
                    httpCon.setRequestMethod("PUT");
                    OutputStreamWriter out = new OutputStreamWriter(
                            httpCon.getOutputStream());
                    out.write(obj.toString());
                    out.close();
                    httpCon.getInputStream();
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    Log.e(TAG, e.toString());
                }
            }
        }).start();
    }
}
