package com.group12.common;

import android.graphics.Bitmap;

/**
 * Created by williamhuang on 11/29/15.
 */
public class Artifact {
    private int id;
    private String description;
    private String image_url;
    private String name;
    private int recommendations;
    private Bitmap image;

    public Exhibit getExhibit() {
        return exhibit;
    }

    public void setExhibit(Exhibit exhibit) {
        this.exhibit = exhibit;
    }

    public boolean isDownloading() {
        return downloading;
    }

    public void setDownloading(boolean downloading) {
        this.downloading = downloading;
    }

    public boolean isRecommended() {
        return recommended;
    }

    public void setRecommended(boolean recommended) {
        this.recommended = recommended;
    }

    private Exhibit exhibit;
    private boolean downloading;
    private boolean recommended;

    public Artifact(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Artifact(int id, Exhibit exhibit, String description, String image, String name, int recommendations) {
        this.id = id;
        this.description = description;
        this.image_url = image;
        this.name = name;
        this.exhibit = exhibit;
        this.recommendations = recommendations;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public int getRecommendations() {
        return recommendations;
    }

    public void setRecommendations(int recommendations) {
        this.recommendations = recommendations;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }
}
