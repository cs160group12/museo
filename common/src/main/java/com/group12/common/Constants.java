package com.group12.common;

/**
 * Created by williamhuang on 11/28/15.
 */
public class Constants {
    public static final String BASE_MUSEUM_URL = "http://whispering-brushlands-3458.herokuapp.com/museo/api/v1.0/";
    public static final String SYSTEM_TIME = "/systemTime";

    public static final String MUSEUM_ID = "/museum/id";

    public static final String EXHIBIT_ID = "/exhibit/id";

    public static final String ARTIFACT_PATH = "/wearArtifact";
    public static final String ARTIFACT_ID = "/wearArtifact/id";
    public static final String ARTIFACT_NAME = "/wearArtifact/name";
    public static final String ARTIFACT_IMG = "/wearArtifact/img";
    public static final String ARTIFACT_CACHE = "/wearArtifact/cache";

    public static final String ARTIFACT_NEXT = "/mobileArtifact/next";
    public static final String ARTIFACT_PREV = "/mobileArtifact/prev";
    public static final String ARTIFACT_NEXT_CACHE = "/mobileArtifact/nextcache";
    public static final String ARTIFACT_PREV_CACHE = "/mobileArtifact/prevcache";

    public static final String SHARE_FACEBOOK = "/mobileShare/facebook";
    public static final String SHARE_TWITTER = "/mobileShare/twitter";

}
