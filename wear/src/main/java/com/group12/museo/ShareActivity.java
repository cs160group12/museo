package com.group12.museo;

import android.app.Activity;
import android.os.Bundle;
import android.support.wearable.view.WatchViewStub;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;
import com.group12.common.Constants;

import java.math.BigInteger;

public class ShareActivity extends Activity {

    private int artifact_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share);

        TextView shareTextView = (TextView) findViewById(R.id.shareTextView);
        shareTextView.setTypeface(WearWelcomeActivity.typeface);

        artifact_id = getIntent().getIntExtra(Constants.ARTIFACT_ID, 0);

        ImageButton facebookButton = (ImageButton) findViewById(R.id.facebookButton);
        facebookButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendShareMessage(Constants.SHARE_FACEBOOK);
            }
        });

        ImageButton twitterButton = (ImageButton) findViewById(R.id.twitterButton);
        twitterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendShareMessage(Constants.SHARE_TWITTER);
            }
        });
    }

    private void sendShareMessage(final String changePath) {
        new Thread( new Runnable() {
            @Override
            public void run() {
                NodeApi.GetConnectedNodesResult nodes = Wearable.NodeApi.getConnectedNodes( WearWelcomeActivity.mGoogleApiClient ).await();
                for(Node node : nodes.getNodes()) {
                    MessageApi.SendMessageResult result = Wearable.MessageApi.sendMessage(
                            WearWelcomeActivity.mGoogleApiClient, node.getId(), changePath, BigInteger.valueOf(artifact_id).toByteArray()).await();
                }
            }
        }).start();
    }
}
