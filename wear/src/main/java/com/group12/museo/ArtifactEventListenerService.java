package com.group12.museo;

import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.data.FreezableUtils;
import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.Wearable;
import com.google.android.gms.wearable.WearableListenerService;
import com.group12.common.Constants;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class ArtifactEventListenerService extends WearableListenerService {
    private static final String TAG = "ArtifactEvent";

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "Listener Service Started");
    }

    @Override
    public void onDataChanged(DataEventBuffer dataEvents) {
        if (Log.isLoggable(TAG, Log.DEBUG)) {
            Log.d(TAG, "onDataChanged: " + dataEvents);
        }

        final List events = FreezableUtils
                .freezeIterable(dataEvents);

        if (WearWelcomeActivity.mGoogleApiClient == null) {
            WearWelcomeActivity.mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(Wearable.API).build();
        }

        ConnectionResult connectionResult =
                WearWelcomeActivity.mGoogleApiClient.blockingConnect(10, TimeUnit.SECONDS);

        if (!connectionResult.isSuccess()) {
            Log.e(TAG, "Failed to connect to GoogleApiClient.");
            return;
        }

        for (Object obj: events) {
            DataEvent event = (DataEvent)obj;
            DataMapItem artifactData = DataMapItem.fromDataItem(event.getDataItem());
            Intent i = new Intent(this, WearArtifactActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            i.putExtra(Constants.ARTIFACT_ID, artifactData.getDataMap().getInt(Constants.ARTIFACT_ID));
            i.putExtra(Constants.ARTIFACT_NAME, artifactData.getDataMap().getString(Constants.ARTIFACT_NAME));
            i.putExtra(Constants.ARTIFACT_IMG, artifactData.getDataMap().getAsset(Constants.ARTIFACT_IMG));
            i.putExtra(Constants.ARTIFACT_CACHE, artifactData.getDataMap().getBoolean(Constants.ARTIFACT_CACHE));
            startActivity(i);
        }
    }

}
