package com.group12.museo;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.nfc.Tag;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.wearable.view.WatchViewStub;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.wearable.Asset;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.Wearable;
import com.group12.common.Artifact;
import com.group12.common.Constants;
import com.group12.museo.util.OnSwipeTouchListener;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class WearArtifactActivity extends Activity {

    private String TAG = "WearArtifact";
    private int artifact_id;
    private TextView artifactTextView;
    private ImageView artifactImageView;

    private static Map<String, Artifact> artifactCache = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d(TAG, "Started Wear Artifact Activity");

        Boolean cache = getIntent().getBooleanExtra(Constants.ARTIFACT_CACHE, false);

        String artifact_name;
        if (cache) {
            artifact_name = getIntent().getStringExtra(Constants.ARTIFACT_NAME);
            int aid = getIntent().getIntExtra(Constants.ARTIFACT_ID, 0);
            if (!artifactCache.containsKey(aid+artifact_name)) {

                Asset artifact_img = getIntent().getParcelableExtra(Constants.ARTIFACT_IMG);
                artifactCache.put(aid+artifact_name, new Artifact(aid, artifact_name));

                new LoadImageTask(null, aid+artifact_name).execute(artifact_img);
            }
            return;
        }

        setContentView(R.layout.activity_wear_artifact);

        artifact_id = getIntent().getIntExtra(Constants.ARTIFACT_ID, 0);
        artifactImageView = (ImageView) findViewById(R.id.artifactImageView);

        if (artifactCache.containsKey(artifact_id)) {
            Artifact a = artifactCache.get(artifact_id);
            artifact_name = a.getName();
            artifactImageView.setImageBitmap(a.getImage());
        } else {
            artifact_name = getIntent().getStringExtra(Constants.ARTIFACT_NAME);

            Asset artifact_img = getIntent().getParcelableExtra(Constants.ARTIFACT_IMG);
            artifactCache.put(artifact_id+artifact_name, new Artifact(artifact_id, artifact_name));

            new LoadImageTask(artifactImageView, artifact_id+artifact_name).execute(artifact_img);
        }
        artifactTextView = (TextView) findViewById(R.id.artifactTextView);
        artifactTextView.setText(artifact_name);
        artifactTextView.setTypeface(WearWelcomeActivity.typeface);

        final float[] coord = new float[2];
        artifactImageView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        coord[0] = event.getX();
                        coord[1] = event.getY();

                        Log.d(TAG, "Action was DOWN");
                        break;
                    case MotionEvent.ACTION_UP:
                        float finalX = event.getX();
                        float finalY = event.getY();
                        float initialX = coord[0];
                        float initialY = coord[1];

                        float XDelta = Math.abs(finalX-initialX);
                        float YDelta = Math.abs(finalY-initialY);

                        Log.d(TAG, "Action was UP <"+XDelta + ","+YDelta+">");

                        if (YDelta > XDelta) {
                            if (YDelta < 120) {
                                return true;
                            }
                            if (initialY < finalY) {
                                Log.d(TAG, "Up to Down swipe performed");
                                sendArtifactChangeMessage(Constants.ARTIFACT_PREV);
                            } else {
                                Log.d(TAG, "Down to Up swipe performed");
                                sendArtifactChangeMessage(Constants.ARTIFACT_NEXT);
                            }
                        } else {
                            if (XDelta < 120) {
                                return true;
                            }
                            if (initialX < finalX) {
                                Log.d(TAG, "Left to Right swipe performed");
                                finish();
                            } else {
                                Log.d(TAG, "Right to Left swipe performed");
                                Intent i = new Intent(getApplicationContext(), ShareActivity.class);
                                i.putExtra(Constants.ARTIFACT_ID, artifact_id);
                                startActivity(i);
                            }
                        }
                        break;
                }
                return true;
            }
        });
    }

    private void sendArtifactChangeMessage(final String changePath) {
        new Thread( new Runnable() {
            @Override
            public void run() {
                NodeApi.GetConnectedNodesResult nodes = Wearable.NodeApi.getConnectedNodes( WearWelcomeActivity.mGoogleApiClient ).await();
                for(Node node : nodes.getNodes()) {
                    MessageApi.SendMessageResult result = Wearable.MessageApi.sendMessage(
                            WearWelcomeActivity.mGoogleApiClient, node.getId(), changePath, null).await();
                }
            }
        }).start();
    }

    private class LoadImageTask extends AsyncTask<Asset, Void, Bitmap> {
        ImageView im;
        String aid;

        public LoadImageTask(ImageView im, String aid) {
            this.im = im;
            this.aid = aid;
        }

        protected Bitmap doInBackground(Asset... assets) {
            InputStream assetInputStream = Wearable.DataApi.getFdForAsset(
                    WearWelcomeActivity.mGoogleApiClient, assets[0]).await().getInputStream();
            return BitmapFactory.decodeStream(assetInputStream);
        }

        protected void onPostExecute(Bitmap result){
            if (im != null) {
                im.setImageBitmap(result);
            }
            artifactCache.get(aid).setImage(result);
        }
    }
}
