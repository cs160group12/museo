package com.group12.museo;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;

import com.group12.common.Constants;
import com.group12.common.Exhibit;
import com.group12.common.Museum;
import com.group12.museo.utils.ExhibitListAdapter;


import java.util.LinkedList;
import java.util.List;

public class ExhibitActivity extends AppCompatActivity {

    private static ExhibitListAdapter exhibitListAdapter;
    private List<Exhibit> exhibits;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exhibit);

        int museum_id = getIntent().getIntExtra(Constants.MUSEUM_ID, 0);
        getExhibitsList(museum_id);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
    }

    private void getExhibitsList(int museum_id) {
        Museum m = WelcomeActivity.museumMap.get(museum_id);
        exhibits = new LinkedList<>();
        exhibits.addAll(m.getExhibits());

        setTitle(m.getTitle());

        setupExhibitList();
    }

    private void setupExhibitList() {
        ListView exhibitListView = (ListView)findViewById(R.id.exhibitListView);
        exhibitListAdapter = new ExhibitListAdapter(this, R.layout.row_item_exhibit_list, exhibits);
        exhibitListView.setAdapter(exhibitListAdapter);
        exhibitListView.setItemsCanFocus(false);
        exhibitListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(getApplicationContext(), ArtifactActivity.class);
                i.putExtra(Constants.EXHIBIT_ID, exhibits.get(position).getId());
                i.putExtra(Constants.MUSEUM_ID, exhibits.get(position).getMuseum().getId());
                startActivity(i);
            }
        });
    }
}
