package com.group12.museo;

import android.content.Intent;
import android.util.Log;

import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.WearableListenerService;
import com.group12.common.Constants;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import java.net.MalformedURLException;
import java.net.URL;

public class ArtifactListenerService extends WearableListenerService {
    private static final String TAG = "ArtifactListener";

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "Listener Service Started");
    }

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        if (messageEvent.getPath().equalsIgnoreCase(Constants.ARTIFACT_NEXT) ) {
            ArtifactActivity.getNextArtifact();
        } else if (messageEvent.getPath().equalsIgnoreCase(Constants.ARTIFACT_PREV)) {
            ArtifactActivity.getPrevArtifact();
        } else if (messageEvent.getPath().equalsIgnoreCase(Constants.ARTIFACT_NEXT_CACHE)) {
            ArtifactActivity.getNextArtifactCache();
        } else if (messageEvent.getPath().equalsIgnoreCase(Constants.SHARE_FACEBOOK)) {
            // TODO: FACEBOOK INTEGRATION
            Intent intent = new Intent(this, ArtifactActivity.class);
            intent.putExtra("Start", "ShareFB");
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK );
            startActivity(intent);
        } else if (messageEvent.getPath().equalsIgnoreCase(Constants.SHARE_TWITTER)) {
            Log.d(TAG, "Twitter Share");
            Intent intent = null;
            try {
                intent = new TweetComposer.Builder(this)
                        .text("I think you guys should see this: ")
                        .url(new URL("http://www.museo.com/artifact/123/"))
                        .createIntent();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else {
            super.onMessageReceived( messageEvent );
        }
    }


}
