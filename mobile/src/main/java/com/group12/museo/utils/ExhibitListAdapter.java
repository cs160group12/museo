package com.group12.museo.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.group12.common.Constants;
import com.group12.common.Exhibit;
import com.group12.common.ImageUtils;
import com.group12.museo.R;
import com.group12.museo.WelcomeActivity;

import java.util.List;

/**
 * Created by williamhuang on 12/1/15.
 */
public class ExhibitListAdapter extends ArrayAdapter {
    private static final String TAG = "ExhibitList";

    private int resource;
    private LayoutInflater inflater;
    Context context;
    List<Exhibit> exhibits;

    public ExhibitListAdapter(Context ctx, int resourceId, List<Exhibit> exhibits) {
        super(ctx, resourceId, exhibits);
        resource = resourceId;
        inflater = LayoutInflater.from(ctx);
        context = ctx;
        this.exhibits = exhibits;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(resource, null);

        ImageView exhibitImageView;
        final TextView recTextView;
        final Exhibit e;

        e = exhibits.get(position);

        exhibitImageView = (ImageView) convertView.findViewById(R.id.exhibitBG);
        Bitmap image = e.getImage();
        if (image == null) {
            if (!e.isDownloading()) {
                new DownloadExhibitTask(exhibitImageView, e, this).execute(Constants.BASE_MUSEUM_URL + e.getImage_url());
                e.setDownloading(true);
            }
        } else {
            exhibitImageView.setImageBitmap(image);
        }

        TextView exhibitNameTextView = (TextView) convertView.findViewById(R.id.exhibitNameTextView);
        exhibitNameTextView.setText(e.getName());
        exhibitNameTextView.setTypeface(WelcomeActivity.typeface);

        recTextView = (TextView) convertView.findViewById(R.id.recExhibitTextView);
        recTextView.setText(e.getRecommendations()+" recommend this");
        recTextView.setTypeface(WelcomeActivity.typeface);

        ToggleButton exhibitToggleButton = (ToggleButton) convertView.findViewById(R.id.exhibitToggleButton);
        exhibitToggleButton.setChecked(e.isRecommended());
        exhibitToggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                int newRec;
                if (isChecked) {
                    newRec = e.getRecommendations() + 1;
                    e.setRecommended(true);
                } else {
                    newRec = e.getRecommendations() - 1;
                    e.setRecommended(false);
                }
                e.setRecommendations(newRec);
                recTextView.setText(e.getRecommendations() + " recommend this");
                ImageUtils.postRecommendation(Constants.BASE_MUSEUM_URL + "museums/" + e.getMuseum().getId() + "/exhibits/" + e.getId() + "/recommendations", isChecked ? 1 : -1);
                notifyDataSetChanged();
            }
        });

        return convertView;
    }

    public static class DownloadExhibitTask extends AsyncTask<String, Void, Bitmap> {

        ImageView im;
        Exhibit e;
        ExhibitListAdapter mla;

        public DownloadExhibitTask(ImageView im, Exhibit e, ExhibitListAdapter mla) {
            this.im = im;
            this.e = e;
            this.mla = mla;
        }

        protected Bitmap doInBackground(String... urls) {
            Log.d(TAG, "Downloading exhibit image: "+urls[0]);
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            mIcon11 = ImageUtils.decodeSampledBitmapFromStream(urldisplay, 800, 800);
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result){
            e.setImage(result);
            e.setDownloading(false);
            Log.d(TAG, "Done downloading");
            im.setImageBitmap(result);
            mla.notifyDataSetChanged();
        }
    }
}
