package com.group12.museo.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.group12.common.Artifact;
import com.group12.common.Constants;
import com.group12.common.ImageUtils;
import com.group12.museo.R;
import com.group12.museo.WelcomeActivity;

import java.util.List;

/**
 * Created by williamhuang on 12/1/15.
 */
public class ArtifactListAdapter extends ArrayAdapter {
    private static final String TAG = "ArtifactList";

    private int resource;
    private LayoutInflater inflater;
    Context context;
    List<Artifact> artifacts;

    public ArtifactListAdapter(Context ctx, int resourceId, List<Artifact> artifacts) {
        super(ctx, resourceId, artifacts);
        resource = resourceId;
        inflater = LayoutInflater.from(ctx);
        context = ctx;
        this.artifacts = artifacts;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(resource, null);

        final TextView recTextView;
        final Artifact a;

        a = artifacts.get(position);

        Bitmap image = a.getImage();
        ImageView artifactBG = (ImageView) convertView.findViewById(R.id.artifactBG);
        if (image == null) {
            if (!a.isDownloading()) {
                new DownloadArtifactTask(artifactBG, a, this).execute(Constants.BASE_MUSEUM_URL + a.getImage_url());
                a.setDownloading(true);
            }
        } else {
            artifactBG.setImageBitmap(image);
        }

        TextView artifactNameTextView = (TextView) convertView.findViewById(R.id.artifactNameTextView);
        artifactNameTextView.setText(a.getName());
        artifactNameTextView.setTypeface(WelcomeActivity.typeface);

        TextView artifactDescTextView = (TextView) convertView.findViewById(R.id.artifactDescTextView);
        artifactDescTextView.setText(a.getDescription());
        artifactDescTextView.setTypeface(WelcomeActivity.typeface);



        recTextView = (TextView) convertView.findViewById(R.id.recArtifactTextView);
        recTextView.setText(a.getRecommendations()+" recommend this");
        recTextView.setTypeface(WelcomeActivity.typeface);

        ToggleButton artifactToggleButton = (ToggleButton) convertView.findViewById(R.id.artifactToggleButton);
        artifactToggleButton.setChecked(a.isRecommended());
        artifactToggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                int newRec;
                if (isChecked) {
                    newRec = a.getRecommendations() + 1;
                    a.setRecommended(true);
                } else {
                    newRec = a.getRecommendations() - 1;
                    a.setRecommended(false);
                }
                a.setRecommendations(newRec);
                recTextView.setText(a.getRecommendations() + " recommend this");
                ImageUtils.postRecommendation(Constants.BASE_MUSEUM_URL + "museums/" + a.getExhibit().getMuseum().getId() + "/exhibits/" + a.getExhibit().getId() + "/artifacts/" + a.getId() + "/recommendations", isChecked ? 1 : -1);
                notifyDataSetChanged();
            }
        });

        return convertView;
    }

    public static class DownloadArtifactTask extends AsyncTask<String, Void, Bitmap> {

        ImageView im;
        Artifact a;
        ArtifactListAdapter mla;

        public DownloadArtifactTask(ImageView im, Artifact a, ArtifactListAdapter mla) {
            this.a = a;
            this.mla = mla;
            this.im = im;
        }

        protected Bitmap doInBackground(String... urls) {
            Log.d(TAG, "Downloading artifact image: "+urls[0]);
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            mIcon11 = ImageUtils.decodeSampledBitmapFromStream(urldisplay, 800, 800);
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result){
            a.setImage(result);
            a.setDownloading(false);
            Log.d(TAG, "Done downloading");
            im.setImageBitmap(result);
            mla.notifyDataSetChanged();
        }
    }
}
