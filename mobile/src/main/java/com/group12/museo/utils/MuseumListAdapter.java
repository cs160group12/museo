package com.group12.museo.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.group12.common.Constants;
import com.group12.common.Exhibit;
import com.group12.common.ImageUtils;
import com.group12.common.Museum;
import com.group12.museo.R;
import com.group12.museo.WelcomeActivity;

import java.util.List;

/**
 * Created by williamhuang on 12/1/15.
 */
public class MuseumListAdapter extends ArrayAdapter {
    private static final String TAG = "MuseumList";

    private int resource;
    private LayoutInflater inflater;
    Context context;
    List<Museum> museums;

    public MuseumListAdapter(Context ctx, int resourceId, List<Museum> museums) {
        super(ctx, resourceId, museums);
        resource = resourceId;
        inflater = LayoutInflater.from(ctx);
        context = ctx;
        this.museums = museums;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(resource, null);

        ImageView museumImageView;
        final TextView recTextView;
        final Museum m;

        m = museums.get(position);

        museumImageView = (ImageView) convertView.findViewById(R.id.museumBG);
        Bitmap image = m.getImage();
        if (image == null) {
            if (!m.isDownloading()) {
                new DownloadMuseumTask(museumImageView, m, this).execute(Constants.BASE_MUSEUM_URL + m.getImage_url());
                m.setDownloading(true);
            }
        } else {
            museumImageView.setImageBitmap(image);
        }

        TextView museumNameTextView = (TextView) convertView.findViewById(R.id.museumNameTextView);
        museumNameTextView.setText(m.getTitle());
        museumNameTextView.setTypeface(WelcomeActivity.typeface);

        recTextView = (TextView) convertView.findViewById(R.id.recMuseumTextView);
        recTextView.setText(m.getRecommendations()+" recommend this");
        recTextView.setTypeface(WelcomeActivity.typeface);

        ToggleButton museumToggleButton = (ToggleButton) convertView.findViewById(R.id.museumToggleButton);
        museumToggleButton.setChecked(m.isRecommended());
        museumToggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                int newRec;
                if (isChecked) {
                    newRec = m.getRecommendations() + 1;
                    m.setRecommended(true);
                } else {
                    newRec = m.getRecommendations() - 1;
                    m.setRecommended(false);
                }
                m.setRecommendations(newRec);
                recTextView.setText(m.getRecommendations() + " recommend this");
                ImageUtils.postRecommendation(Constants.BASE_MUSEUM_URL + "museums/" + m.getId() + "/recommendations", isChecked ? 1 : -1);
                notifyDataSetChanged();
            }
        });

        return convertView;
    }

    public static class DownloadMuseumTask extends AsyncTask<String, Void, Bitmap> {

        ImageView im;
        Museum m;
        MuseumListAdapter mla;

        public DownloadMuseumTask(ImageView im, Museum m, MuseumListAdapter mla) {
            this.im = im;
            this.m = m;
            this.mla = mla;
        }

        protected Bitmap doInBackground(String... urls) {
            Log.d(TAG, "Downloading museum image: "+urls[0]);
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            mIcon11 = ImageUtils.decodeSampledBitmapFromStream(urldisplay, 800, 800);
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result){
            m.setImage(result);
            m.setDownloading(false);
            Log.d(TAG, "Done downloading");
            im.setImageBitmap(result);
            mla.notifyDataSetChanged();
        }
    }
}
