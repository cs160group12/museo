package com.group12.museo;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.ShareApi;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.Asset;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.Wearable;
import com.group12.common.Artifact;
import com.group12.common.Constants;
import com.group12.common.Exhibit;
import com.group12.common.ImageUtils;
import com.group12.common.Museum;
import com.group12.museo.utils.ArtifactListAdapter;
import com.group12.museo.utils.ExhibitListAdapter;

import java.io.ByteArrayOutputStream;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class ArtifactActivity extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{

    private static final String TAG = "Artifact";
    public static GoogleApiClient mGoogleApiClient;
    private static PutDataMapRequest dataMapRequest;

    private static int curr_artifact = 0;
    private static ArtifactListAdapter artifactListAdapter;
    private static List<Artifact> artifacts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(this.getApplicationContext());

        callbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        handlePendingAction();
                    }

                    @Override
                    public void onCancel() {
                        if (pendingAction != PendingAction.NONE) {
                            showAlert();
                            pendingAction = PendingAction.NONE;
                        }
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        if (pendingAction != PendingAction.NONE
                                && exception instanceof FacebookAuthorizationException) {
                            showAlert();
                            pendingAction = PendingAction.NONE;
                        }
                    }

                    private void showAlert() {
                        new AlertDialog.Builder(ArtifactActivity.this)
                                .setTitle("Cancelled")
                                .setMessage("Unable to perform selected action because permissions were not granted.")
                                .setPositiveButton("OK", null)
                                .show();
                    }
                });

        shareDialog = new ShareDialog(this);
        shareDialog.registerCallback(
                callbackManager,
                shareCallback);



        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {
                // It's possible that we were waiting for Profile to be populated in order to
                // post a status update.
                handlePendingAction();
            }
        };



        // Can we present the share dialog for regular links?
        canPresentShareDialog = ShareDialog.canShow(
                ShareLinkContent.class);

        setContentView(R.layout.activity_artifact);

        int museum_id = getIntent().getIntExtra(Constants.MUSEUM_ID, 0);
        int exhibit_id = getIntent().getIntExtra(Constants.EXHIBIT_ID, 0);
        getArtifactsList(museum_id, exhibit_id);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);

        //Connect the GoogleApiClient
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    public static void getNextArtifact() {
        sendArtifactToWear(artifacts.get(curr_artifact), false);
        curr_artifact = (curr_artifact + 1) % artifacts.size();
    }

    public static void getNextArtifactCache() {
        sendArtifactToWear(artifacts.get(curr_artifact), true);
        curr_artifact = (curr_artifact + 1) % artifacts.size();
    }

    public static void getPrevArtifact() {
        sendArtifactToWear(artifacts.get(curr_artifact), false);
        curr_artifact = (curr_artifact - 1 + artifacts.size()) % artifacts.size();
    }


    /**
     * Send message to mobile handheld
     */
    private static void sendArtifactToWear(Artifact artifact, boolean cache) {
        if (mGoogleApiClient.isConnected() && artifact.getImage() != null) {
            dataMapRequest = PutDataMapRequest.create(Constants.ARTIFACT_PATH);
            dataMapRequest.getDataMap().putLong(Constants.SYSTEM_TIME, System.currentTimeMillis());
            dataMapRequest.getDataMap().putInt(Constants.ARTIFACT_ID, artifact.getId());
            dataMapRequest.getDataMap().putString(Constants.ARTIFACT_NAME, artifact.getName());
            dataMapRequest.getDataMap().putBoolean(Constants.ARTIFACT_CACHE, cache);
            dataMapRequest.getDataMap().putAsset(Constants.ARTIFACT_IMG, createAssetFromBitmap(artifact.getImage()));

            Log.d(TAG, "Sending Artifact to Wear");

            PutDataRequest putDataRequest = dataMapRequest.asPutDataRequest();
            Wearable.DataApi.putDataItem(mGoogleApiClient, putDataRequest);
        }
    }

    private void getArtifactsList(int museum_id, int exhibit_id) {
        List<Exhibit> exhibits = WelcomeActivity.museumMap.get(museum_id).getExhibits();
        for (Exhibit e: exhibits) {
            if (e.getId() == exhibit_id) {
                artifacts = new LinkedList<>();
                artifacts.addAll(e.getArtifacts());
                setTitle(e.getName());
            }
        }

        setupArtifactList();
    }

    private void setupArtifactList() {
        ListView artifactListView = (ListView)findViewById(R.id.artifactListView);
        artifactListAdapter = new ArtifactListAdapter(this, R.layout.row_item_artifact_list_v2, artifacts);
        artifactListView.setAdapter(artifactListAdapter);
        artifactListView.setItemsCanFocus(false);
        artifactListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                curr_artifact = position;
                getNextArtifact();
            }
        });
    }

    private static Asset createAssetFromBitmap(Bitmap bitmap) {
        final ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteStream);
        return Asset.createFromBytes(byteStream.toByteArray());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        Log.d("FB", "FACEBOOK EVENT INTENT FOUND********************************");
        if (intent.getStringExtra("Start").equals("ShareFB")) {
            postStatusUpdateInit();
        }
    }

    private PendingAction pendingAction = PendingAction.NONE;
    private boolean canPresentShareDialog;
    private CallbackManager callbackManager;
    private ProfileTracker profileTracker;
    private ShareDialog shareDialog;
    private enum PendingAction {
        NONE,
        POST_STATUS_UPDATE
    }
    private FacebookCallback<Sharer.Result> shareCallback = new FacebookCallback<Sharer.Result>() {
        @Override
        public void onCancel() {
            Log.d("HelloFacebook", "Canceled");
        }

        @Override
        public void onError(FacebookException error) {
            Log.d("HelloFacebook", String.format("Error: %s", error.toString()));
            String title = "Error";
            String alertMessage = error.getMessage();
            showResult(title, alertMessage);
        }

        @Override
        public void onSuccess(Sharer.Result result) {
            Log.d("HelloFacebook", "Success!");
            if (result.getPostId() != null) {
                String title = "Success!";
                showResult(title, null);
            }
        }

        private void showResult(String title, String alertMessage) {
            new AlertDialog.Builder(ArtifactActivity.this)
                    .setTitle(title)
                    .setMessage(alertMessage)
                    .setPositiveButton("OK", null)
                    .show();
        }
    };


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        profileTracker.stopTracking();
    }

    private void handlePendingAction() {
        PendingAction previouslyPendingAction = pendingAction;
        pendingAction = PendingAction.NONE;

        switch (previouslyPendingAction) {
            case NONE:
                break;
            case POST_STATUS_UPDATE:
                postStatusUpdate();
                break;
        }
    }

    private void postStatusUpdateInit() {
        performPublish(PendingAction.POST_STATUS_UPDATE, canPresentShareDialog);
    }

    private void postStatusUpdate() {
        Profile profile = Profile.getCurrentProfile();
        ShareLinkContent linkContent = new ShareLinkContent.Builder()
                .setContentTitle("Museo")

                .setContentDescription(
                        "Museo helps you find the best museums have to offer")
                .setContentUrl(Uri.parse("museo.com"))
                .build();
        if (canPresentShareDialog) {
            shareDialog.show(linkContent);
        } else if (profile != null && hasPublishPermission()) {
            ShareApi.share(linkContent, shareCallback);
        } else {
            pendingAction = PendingAction.POST_STATUS_UPDATE;
        }
    }

    private boolean hasPublishPermission() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        return accessToken != null && accessToken.getPermissions().contains("publish_actions");
    }

    private void performPublish(PendingAction action, boolean allowNoToken) {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        if (accessToken != null || allowNoToken) {
            pendingAction = action;
            handlePendingAction();
        }
    }
    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }


    @Override
    public void onConnected(Bundle bundle) {
        Log.d(TAG, "onConnected: " + bundle);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }
}
