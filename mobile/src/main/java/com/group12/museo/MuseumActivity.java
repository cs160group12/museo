package com.group12.museo;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.facebook.FacebookSdk;
import com.group12.common.Constants;
import com.group12.common.Museum;
import com.group12.common.TypefaceSpan;
import com.group12.museo.utils.MuseumListAdapter;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;

import java.util.LinkedList;
import java.util.List;

import io.fabric.sdk.android.Fabric;

public class MuseumActivity extends AppCompatActivity {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "eh5lRWWABULUWUZX4eZc86zQd";
    private static final String TWITTER_SECRET = "ClAnKha1hZKnWXrCnbIyWh5BItJ71jBYqEHXyRhAo77bBvBPUy";

    private static MuseumListAdapter museumListAdapter;
    private List<Museum> museums;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_museum);

        SpannableString s = new SpannableString("museo");
        TypefaceSpan ts = new TypefaceSpan(this, "Righteous-Regular.ttf");
        s.setSpan(ts, 0, s.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);


// Update the action bar title with the TypefaceSpan instance
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(s);

        startSocialMediaServices();
        getMuseumsList();
    }

    private void startSocialMediaServices() {
        FacebookSdk.sdkInitialize(this.getApplicationContext());
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));
    }

    private void getMuseumsList() {
        museums = new LinkedList<>();
        museums.addAll(WelcomeActivity.museumMap.values());

        setupMuseumList();
    }

    private void setupMuseumList() {
        ListView museumListView = (ListView)findViewById(R.id.museumListView);
        museumListAdapter = new MuseumListAdapter(this, R.layout.row_item_museum_list, museums);
        museumListView.setAdapter(museumListAdapter);
        museumListView.setItemsCanFocus(false);
        museumListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(getApplicationContext(), ExhibitActivity.class);
                i.putExtra(Constants.MUSEUM_ID, museums.get(position).getId());
                startActivity(i);
            }
        });
    }
}
