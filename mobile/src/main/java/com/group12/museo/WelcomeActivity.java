package com.group12.museo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.internal.LinkedHashTreeMap;
import com.group12.common.Artifact;
import com.group12.common.Constants;
import com.group12.common.Exhibit;
import com.group12.common.Museum;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WelcomeActivity extends AppCompatActivity {

    public static Map<Integer, Museum> museumMap;
    public static Typeface typeface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        typeface = Typeface.createFromAsset(getAssets(), "fonts/Lato-Semibold.ttf");

        TextView welcomeTextView = (TextView) findViewById(R.id.welcomeTextView);
        welcomeTextView.setTypeface(typeface);
        welcomeTextView.setText("Loading, please wait...");

        populateMuseums();
    }

    private void populateMuseums() {
        museumMap = new LinkedHashMap<>();
        new RetrieveMuseumsTask().execute(Constants.BASE_MUSEUM_URL + "museums");
    }

    public class RetrieveMuseumsTask extends AsyncTask<String, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected Boolean doInBackground(String... url) {
            final String TAG = "PopulateMuseums";

            try {
                InputStream response = new URL(url[0]).openStream();
                StringBuffer jsonBuffer = new StringBuffer();
                int c;
                while ((c = response.read()) != -1) {
                    jsonBuffer.append((char) c);
                }
                JSONObject json = new JSONObject(jsonBuffer.toString());
                JSONArray museums = json.getJSONArray("museums");
                for (int i = 0; i < museums.length(); i++) {
                    JSONObject museum = museums.getJSONObject(i);

                    String description = museum.getString("description");
                    int museum_id = museum.getInt("id");
                    String image = museum.getString("image");
                    int recommendations = museum.getInt("recommendations");
                    String title = museum.getString("title");

                    Log.d(TAG, "Loading "+title);

                    Museum m = new Museum(museum_id, description, image, title, recommendations);

                    JSONArray exhibits = museum.getJSONArray("exhibits");
                    for (int j = 0; j < exhibits.length(); j++) {
                        JSONObject exhibit = exhibits.getJSONObject(j);

                        String eDescription = exhibit.getString("description");
                        if (!exhibit.has("exhibit_id"))
                            continue;
                        int exhibit_id = exhibit.getInt("exhibit_id");
                        String eImage = exhibit.getString("image");
                        String name = exhibit.getString("name");
                        int eRecommendations = exhibit.getInt("recommendations");

                        Exhibit e = new Exhibit(exhibit_id, m, eDescription, eImage, name, eRecommendations);

                        JSONArray artifacts = exhibit.getJSONArray("artifacts");
                        for (int k = 0; k < artifacts.length(); k++) {
                            JSONObject artifact = artifacts.getJSONObject(k);

                            String aDescription = artifact.getString("description");
                            int artifact_id = artifact.getInt("artifact_id");
                            String aImage = artifact.getString("image");
                            String aName = artifact.getString("name");
                            int aRecommendations = artifact.getInt("recommendations");

                            Artifact a = new Artifact(artifact_id, e, aDescription, aImage, aName, aRecommendations);
                            e.addArtifact(a);
                        }
                        m.addExhibit(e);
                    }
                    museumMap.put(museum_id, m);
                }
            } catch (IOException | JSONException e) {
                Log.e(TAG, e.getMessage());
                return Boolean.FALSE;
            }

            return Boolean.TRUE;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);

            TextView welcomeTextView = (TextView) findViewById(R.id.welcomeTextView);
            welcomeTextView.setText("Tap anywhere to continue!");

            RelativeLayout loadingLayout = (RelativeLayout) findViewById(R.id.loadingLayout);
            loadingLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(getApplicationContext(), MuseumActivity.class);
                    startActivity(i);
                }
            });
        }
    }
}
